<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'scZ<x H@QU}X9Gvi-qL^a?$D%W5HCUlj>~#i`XUe%.NgB-y++YAA+|9MiQ|o=T0[');
define('SECURE_AUTH_KEY',  'T2yg$c/^6MldnFi$tWWnzaR|&Bc!0JV4vgnDv^p@0AZA:v,/w7T}>-4m8rh@T=,.');
define('LOGGED_IN_KEY',    'lI*SiFOc~U8]7uav8X-`VGSiWJ%:0H|n~|qpS6`DEwX7}5f+{gH@|K?dYDO0_?Lj');
define('NONCE_KEY',        'RFGj5q|B9(S>1p[r[d+Th3fI!u,-aQ+mg)90q5r fIM!BHMp$PMMLguH(r]<<SYV');
define('AUTH_SALT',        ',B4*g|l/:)9<(G*W4kkUF>~lU$`ZqLBUTqc^iKyeAhzf_+Cb<~[evr|u<9TvBWXO');
define('SECURE_AUTH_SALT', 'v[WThmT+=!.tk@G|:6`.ZQ}glkD{+`p;0LNV%p/W}Jt@Bf,),Yfs$MoyIcnOuU}f');
define('LOGGED_IN_SALT',   '?0Shn[ aEHa>hLtv%hbhxgYuv&C_^-GmnLEIn|]F+Z?mcRI&ge>}-:tt>P:+*z8]');
define('NONCE_SALT',       '@nYc{.x+En---O?W*d@+B4?[FR?kyJf-^KfV|S`w]?1?@:zp^jG}-{Fs!c*u|MfM');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
