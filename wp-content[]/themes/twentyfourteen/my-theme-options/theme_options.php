<?php
//Theme option pannel
/** Step 2 (from text above). */
add_action( 'admin_menu', 'my_theme_options_menu' );

/** Step 1. */
function my_theme_options_menu() {
	add_options_page( 'Theme Options', 'Theme Options', 'manage_options', 'custom-theme-options', 'register_option_fields' );
	//call register settings function
	add_action( 'admin_init', 'register_option_settings' );
}
//register our settings
function register_option_settings(){
	register_setting( 'register_option_fields', 'new_option_name' );
	register_setting( 'register_option_fields', 'some_other_option' );
	register_setting( 'register_option_fields', 'option_etc' );
}
/************************************
 Source code for Media uploader
************************************/
// jQuery
function theme_options_page(){
wp_enqueue_script('jquery');
// This will enqueue the Media Uploader script
wp_enqueue_media();
// And let's not forget the script we wrote earlier
wp_register_script( 'custom-script', get_template_directory_uri().'/my-theme-options/custom-script.js','','', true );
wp_enqueue_script('custom-script');
}
add_action( 'admin_enqueue_scripts', 'theme_options_page' );
/** Step 3. */
function register_option_fields() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
 } ?>
	<div class="wrap">
    <h2>My Custom theme options</h2>
	<form method="post" action="options.php">
      <?php settings_fields( 'register_option_fields' ); 
	        do_settings_sections( 'register_option_fields' );
	  
	  ?>
      <table class="form-table">
        <tr valign="top">
        <th scope="row">New Option Name</th>
        <td><input type="text" name="new_option_name" value="<?php echo esc_attr( get_option('new_option_name') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Some Other Option</th>
        <td><input type="text" name="some_other_option" value="<?php echo esc_attr( get_option('some_other_option') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Uploading an image using Media Uploader</th>
        <td><input type="file" name="upload-btn" id="upload-btn" class="button-secondary" value="Upload Image"></td>
        </tr>
    </table>
      <?php submit_button(); ?>
    </form>
	</div>
<?php }?>

<!--<div class="wrap">
<h2>Your Plugin Page Title</h2>
</div>
-->